package com.example.gridfsoperations.service;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.stereotype.Service;

import com.example.gridfsoperations.StorageObject;

@Service
public class MongoStorageService {

	@Autowired
	private GridFsOperations fs;

	public void store(StorageObject object) {

		try (InputStream inputStream = object.getInputStream()) {
			fs.store(inputStream, object.getUuid());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public StorageObject get(String fileName) {
		try {
			return initStorageObject(fs.getResource(fileName));
		} catch (IllegalStateException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	private StorageObject initStorageObject(GridFsResource resource) throws IllegalStateException, IOException {
		return new StorageObject(resource.getFilename(), resource.contentLength(), resource.getInputStream());
	}
}
