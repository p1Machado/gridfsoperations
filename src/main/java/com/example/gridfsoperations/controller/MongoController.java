package com.example.gridfsoperations.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.gridfsoperations.StorageObject;
import com.example.gridfsoperations.service.MongoStorageService;

@RestController
public class MongoController {

	@Autowired
	private MongoStorageService storageService;

//	@PostMapping("/files/upload")
//	public void upload(@RequestParam("file") MultipartFile file) {
//		storageService.store(new StorageObject(UUID.randomUUID().toString(), file));
//	}

	@PostMapping("/files/upload")
	public void upload(HttpServletRequest request) throws FileUploadException, IOException {

		ServletFileUpload upload = new ServletFileUpload();

		// NOTE: We're doing no real validation here, this is just for test purposes
		FileItemIterator iter = upload.getItemIterator(request);
		if (!iter.hasNext()) {
			throw new FileUploadException("FileItemIterator was empty");
		}
		FileItemStream item = iter.next();

		try (InputStream in = item.openStream()) {
			storageService.store(new StorageObject(UUID.randomUUID().toString(), item.openStream()));
			System.out.println("teste");
		}
	}

	@GetMapping("/files/download/{filename}")
	public ResponseEntity<InputStreamResource> download(@PathVariable String filename) throws IOException {

		StorageObject object = storageService.get(filename);

		return ResponseEntity.ok() //
				.contentLength(object.getLength()) //
				.contentType(MediaType.parseMediaType("application/octet-stream")) //
				.body(new InputStreamResource(object.getInputStream())); //
	}

}
