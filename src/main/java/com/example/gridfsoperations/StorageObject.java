package com.example.gridfsoperations;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

public class StorageObject {

	private final String uuid;
	private final Long length;
	private final InputStream stream;
	private final MultipartFile file;

	public StorageObject(String uuid, Long length, InputStream stream) {
		this.uuid = uuid;
		this.stream = stream;
		this.length = length;
		this.file = null;
	}

	public StorageObject(String uuid, InputStream stream) {
		this.uuid = uuid;
		this.stream = stream;
		this.length = null;
		this.file = null;
	}

	public StorageObject(String uuid, MultipartFile file) {
		this.uuid = uuid;
		this.file = file;
		this.length = file.getSize();
		this.stream = null;
	}

	public String getUuid() {
		return uuid;
	}

	public Long getLength() {
		return length;
	}

	public InputStream getInputStream() throws IOException {
		return stream == null ? file.getInputStream() : stream;
	}

}
