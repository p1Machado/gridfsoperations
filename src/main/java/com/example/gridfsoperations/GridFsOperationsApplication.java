package com.example.gridfsoperations;

import java.io.IOException;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@SpringBootApplication
public class GridFsOperationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GridFsOperationsApplication.class, args);
	}

//	@Bean
//	public MultipartResolver multipartResolver() throws IOException {
//		CommonsMultipartResolver resolver = new CommonsMultipartResolver() {
//
//			@Override
//			protected FileUpload newFileUpload(FileItemFactory fileItemFactory) {
//				// TODO Auto-generated method stub
//				return new ServletFileUpload();
//			}
//
//		};
//
//		return resolver;
//	}

}
